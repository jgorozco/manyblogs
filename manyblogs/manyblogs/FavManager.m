//
//  FavManager.m
//  manyblogs
//
//  Created by jose garcia orozco on 17/02/13.
//  Copyright (c) 2013 jgarcia. All rights reserved.
//

#import "FavManager.h"
#define FAVS @"favoritosssss"
@implementation FavManager
+(void)addFav:(NSDictionary *)object{
    NSMutableArray *favs=[[NSMutableArray alloc] initWithArray:[self getFavs]];
    [favs addObject:object];
    [[NSUserDefaults standardUserDefaults]setObject:favs forKey:FAVS];


}
+(void)remFav:(NSDictionary *)object{
     NSMutableArray *favs=[[NSMutableArray alloc] initWithArray:[self getFavs]];
    NSDictionary *Selected;
    for (NSDictionary *d in favs)
    {
        if ([[d objectForKey:@"id"]intValue]==[[object objectForKey:@"id"]intValue]){
            Selected=d;
            
        }
    }
    [favs removeObject:Selected];
    [[NSUserDefaults standardUserDefaults]setObject:favs forKey:FAVS];
    
    
}
+(NSArray *)getFavs{
    return [[NSUserDefaults standardUserDefaults]objectForKey:FAVS];
}
+(BOOL)isFav:(NSDictionary *)object{
    NSMutableArray *favs=[[NSMutableArray alloc] initWithArray:[self getFavs]];
    for (NSDictionary *d in favs)
    {
        if ([[d objectForKey:@"id"]intValue]==[[object objectForKey:@"id"]intValue]){
            return YES;
        }
    }
    return NO;
}
@end
