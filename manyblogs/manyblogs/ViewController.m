//
//  ViewController.m
//  manyblogs
//
//  Created by jose garcia orozco on 12/02/13.
//  Copyright (c) 2013 jgarcia. All rights reserved.
//

#import "ViewController.h"
#import "collectionCell.h"
#import "Constants.h"
#import "COMM.h"
#import "ArticleDTO.h"
#import "ArticleViewController.h"
#import "FavManager.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    currentPage=1;
    descargandoMas=NO;
    noHayMas=NO;
    categorias=[[NSArray alloc]init];
    [self.middleLoadingview setHidden:YES];
    [self.navigationController setNavigationBarHidden:YES];
    NSString *nib=@"collectionCell";
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPhone) {
        nib=@"collectionCell_ipad";
    }
    
    [self.collectionView registerClass:[collectionCell class] forCellWithReuseIdentifier:nib];
    
    [COMM sendGETQuery:[NSString stringWithFormat:BLOG_URL,@"api/get_category_index/"] responseHandler:self reqParams:nil];
    NSString *elementos=[NSString stringWithFormat:@"api/get_recent_posts/?count=%d",PAGINA_IPHONE];
    [self loadMainData:elementos];
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated{
    if (isFavs){
        [self loadFabs];
    }
}

-(void)loadMainData:(NSString *)subquery{
    if ([subquery rangeOfString:@"&page="].location == NSNotFound){
        [self setLoadView:YES];
    }else{
        NSLog(@"Recarga parcial");
    }
    [COMM sendGETQuery:[NSString stringWithFormat:BLOG_URL,subquery] responseHandler:self reqParams:nil];
    
    
    
}

-(void)useResponse:(id)data response:(NSURLResponse *) resp{
    if ([resp.URL.absoluteString isEqualToString:[NSString stringWithFormat:BLOG_URL,@"api/get_category_index/"]])
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
            categorias=[data objectForKey:@"categories"];
            NSLog(@"Cargadas categorias:%d",categorias.count);
            [self.categoriaList reloadData];
        }];
        
    }else{
        [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
            NSArray *diccionarios=[data objectForKey:@"posts"];
            NSMutableArray *listado=nil;
            if ((!diccionarios)||(diccionarios.count==0))
            {
                noHayMas=YES;
                NSLog(@"_______NO QUEDA MASSSSSSS");
            
            }
            if ([resp.URL.absoluteString rangeOfString:@"&page="].location == NSNotFound){
                listado=[[NSMutableArray alloc] init];
                
            }else{
                listado=[[NSMutableArray alloc]initWithArray:items];
            }
            ArticleDTO *articulo;
            for (NSDictionary *d in diccionarios){
                articulo=[[ArticleDTO alloc] initWithDic:d];
                [listado addObject:articulo];
            }
            items=listado;
            [self.collectionView reloadData];
            NSLog(@":::::Elementos actuales::%d",items.count);
            if ([resp.URL.absoluteString rangeOfString:@"&page="].location == NSNotFound){
                [self.collectionView setContentOffset:CGPointMake(0, 0)];
                [self setLoadView:NO];
            }else{
                NSLog(@"Quitando footer!!!");
                 
                [UIView animateWithDuration:0.3
                                      delay:0.0
                                    options: UIViewAnimationCurveEaseOut
                                 animations:^{
                                     CGRect frm=self.middleLoadingview.frame;
                                     frm=CGRectMake(0, frm.origin.y+frm.size.height, frm.size.width, frm.size.height);
                                     self.middleLoadingview.frame=frm;
                                 }
                                 completion:^(BOOL finished){
                                    [self.middleLoadingview setHidden:YES];
                                 }];
                
                
                descargandoMas=NO;
            }
            
        }];
        
        
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *xib=@"ArticleViewController";
     if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
         xib=@"ArticleViewController_ipad";
     
     }
    ArticleViewController *articulo=[[ArticleViewController alloc] initWithNibName:xib bundle:nil];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        [articulo setModalPresentationStyle:UIModalPresentationPageSheet];
    }
    [articulo loadData:[items objectAtIndex:indexPath.row]];
    [self presentModalViewController:articulo animated:YES];
    
    
}

-(void)setLoadView:(BOOL)showOrHide {
    if(showOrHide){
        NSLog(@"MOSTRAMOSO LOADING");
        [self.loadingView setHidden:NO];
        
        
    }else{
        NSLog(@"QUITAMOS LOADING");
        [self.loadingView setHidden:YES];
    }
    
    
}


-(void)useError:(NSError *) error response:(NSURLResponse *) resp{
    NSLog(@"Error data");
    
}



#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [items count];
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *nib=@"collectionCell";
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPhone) {
        nib=@"collectionCell_ipad";
    }
    
    collectionCell *cell =  [cv dequeueReusableCellWithReuseIdentifier:nib forIndexPath:indexPath];
    [cell setData:[items objectAtIndex:indexPath.row]];
    if (indexPath.row>items.count-4){
        if (!descargandoMas){
            NSLog(@"Hay que descargar mas!!!%d",indexPath.row);
            [self loadNextPage];
        }else{
            NSLog(@"Esperando....");
        }
        
    }
    
    return cell;
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // 2
    //   NSLog(@"LAYOUT??");
    int  offset=CELL_WIDHT;
    CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
    if (screenRect.size.height <= 480){
        offset=CELL_WIDHT_4;
    }
    
    CGSize retval = CGSizeMake(offset, CELL_HEIGHT);
    if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPhone) {
        retval = CGSizeMake(CELL_WIDHT_IPAD, CELL_HEIGHT_IPAD);
    }else{
        if (UIDeviceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])) {
            offset=CELL_WIDHT_PORT;
        }
        retval = CGSizeMake(offset, CELL_HEIGHT);
    }
    return retval;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self.collectionView reloadData];
}

// 3
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    //  NSLog(@"Contenedor??");
    int offset=CELL_GAP_LAT_IPH5;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
        if (screenRect.size.height == 568){
            offset=CELL_GAP_LAT_IPH5;
        }else{
            offset=CELL_GAP_LAT_IPH4;
        }
    }else{
        if (UIDeviceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) {
            offset=CELL_GAP_LAT_IPAD;
        }else{
            offset=CELL_GAP_LAT_IPAD_PORT;
        }
    }
    NSLog(@"Offset::%d",offset);
    return UIEdgeInsetsMake(CELL_GAP_TOP, offset, CELL_GAP_BTN, offset);
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCollectionView:nil];
    [self setMainView:nil];
    [self setCategoriaList:nil];
    [self setLoadingView:nil];
    [self setMiddleLoadingview:nil];
    [super viewDidUnload];
}
- (IBAction)showHide:(id)sender {
    if (self.mainView.frame.origin.x==0.0){
        [UIView animateWithDuration:FLOAT_ANIMATION_DURATION
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             [self.mainView setFrame:CGRectMake(OFFSET_X, 0,
                                                                self.mainView.frame.size.width,
                                                                self.mainView.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                         }];
        
        
        
    }else{
        [UIView animateWithDuration:FLOAT_ANIMATION_DURATION
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             [self.mainView setFrame:CGRectMake(0, 0,
                                                                self.mainView.frame.size.width,
                                                                self.mainView.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                         }];
        
        
        
        
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return categorias.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"categoriaCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    NSDictionary *object = [categorias objectAtIndex:indexPath.row];
    //    NSLog(@"comentaro:%@",object);
    NSArray *subvistas=[[cell.subviews objectAtIndex:0] subviews];//[[cell.subviews objectAtIndex:0] subviews];
    for (UIView *v in subvistas){
        switch (v.tag) {
            case 10:{
                UILabel *lbl=(UILabel *)v;
                NSString *s=[object objectForKey:@"title"];
                s=[NSString stringWithFormat:@"%@ (%d)",s,[[object objectForKey:@"post_count"] intValue]];
                if ((s)&&(![s isEqual:[NSNull null]])&&(s!=NULL))
                    [lbl setText:s];
            }
            default:
                break;
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        isFavs=NO;
    if (self.mainView.frame.origin.x!=0.0){
        [UIView animateWithDuration:0.3
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             [self.mainView setFrame:CGRectMake(0, 0,
                                                                self.mainView.frame.size.width,
                                                                self.mainView.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                         }];
        
    }
    NSDictionary *object = [categorias objectAtIndex:indexPath.row];
    currentPage=1;
    descargandoMas=NO;
    noHayMas=NO;
    NSString *queryData=[NSString stringWithFormat:@"category/%@/?json=1&count=%d",[object objectForKey:@"slug"],PAGINA_IPHONE];
    currentTag=[object objectForKey:@"slug"];
    [self loadMainData:queryData];
    
}
-(void)loadNextPage{
    if ((!descargandoMas)&&(!noHayMas)){
         [self.middleLoadingview setHidden:NO];
        [UIView animateWithDuration:0.2
                         animations:^{
                             CGRect frm=self.mainView.frame;
                             frm=CGRectMake(0, 0, frm.size.width, frm.size.height);
                             self.mainView.frame=frm;
        }];
        descargandoMas=YES;
        currentPage=currentPage+1;
         NSString *queryData=[NSString stringWithFormat:@"category/%@/?json=1&count=%d&page=%d",currentTag,PAGINA_IPHONE,currentPage];
        if (!currentTag){
            queryData=[NSString stringWithFormat:@"api/get_recent_posts/?count=%d&page=%d",PAGINA_IPHONE,currentPage];
        }
        [self loadMainData:queryData];
    }else{
        NSLog(@"Esta descargando o NO HAY MAS");
    }
    
    
    
}



- (void)loadFabs {
    NSArray *diccionarios=[FavManager getFavs];
    NSMutableArray *listado=[[NSMutableArray alloc] init];
    ArticleDTO *articulo;
    for (NSDictionary *d in diccionarios){
        articulo=[[ArticleDTO alloc] initWithDic:d];
        [listado addObject:articulo];
    }
    items=listado;
    [self.collectionView reloadData];
}

- (IBAction)clickFavoritos:(id)sender {
    isFavs=YES;
    
    [self loadFabs];
    [UIView animateWithDuration:0.2
                     animations:^{
                         CGRect frm=self.mainView.frame;
                         frm=CGRectMake(0, 0, frm.size.width, frm.size.height);
                         self.mainView.frame=frm;
                     }];

    
}

- (IBAction)clickAllPosts:(id)sender {
    isFavs=NO;
    NSString *elementos=[NSString stringWithFormat:@"api/get_recent_posts/?count=%d",PAGINA_IPHONE];
    currentPage=1;
    descargandoMas=NO;
    noHayMas=NO;
    [self loadMainData:elementos];
}
@end
