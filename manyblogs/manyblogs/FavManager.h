//
//  FavManager.h
//  manyblogs
//
//  Created by jose garcia orozco on 17/02/13.
//  Copyright (c) 2013 jgarcia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FavManager : NSObject
+(void)addFav:(NSDictionary *)object;
+(void)remFav:(NSDictionary *)object;
+(NSArray *)getFavs;
+(BOOL)isFav:(NSDictionary *)object;
@end
