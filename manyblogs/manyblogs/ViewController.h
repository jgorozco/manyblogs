//
//  ViewController.h
//  manyblogs
//
//  Created by jose garcia orozco on 12/02/13.
//  Copyright (c) 2013 jgarcia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "COMMProtocol.h"
@interface ViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,COMMProtocol>
{
    NSMutableArray *items;
    NSArray *categorias;
    NSString *currentTag;
    int currentPage;
    BOOL descargandoMas;
    BOOL noHayMas;
    BOOL isFavs;
    
}
- (IBAction)clickFavoritos:(id)sender;

- (IBAction)clickAllPosts:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *mainView;
- (IBAction)showHide:(id)sender;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UITableView *categoriaList;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
@property (strong, nonatomic) IBOutlet UIView *middleLoadingview;

@end
