//
//  AppDelegate.h
//  manyblogs
//
//  Created by jose garcia orozco on 12/02/13.
//  Copyright (c) 2013 jgarcia. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;

@property (strong, nonatomic) ViewController *viewController;

@end
