//
//  COMMProtocol.h
//  calendamigos
//
//  Created by Jose Garcia Orozco on 05/11/12.
//  Copyright (c) 2012 technoactivity. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol COMMProtocol <NSObject>
-(void)useResponse:(id)data response:(NSURLResponse *) resp;
-(void)useError:(NSError *) error response:(NSURLResponse *) resp;
@end
