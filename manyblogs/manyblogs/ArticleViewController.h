//
//  ArticleViewController.h
//  manyblogs
//
//  Created by jose garcia orozco on 15/02/13.
//  Copyright (c) 2013 jgarcia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleDTO.h"
@interface ArticleViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>
{
    ArticleDTO *articulo;

}

-(void)loadData:(ArticleDTO *)article;

@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewImages;
@property (strong, nonatomic) IBOutlet UIWebView *webViewContent;
@property (strong, nonatomic) IBOutlet UIView *vistaContenido;

- (IBAction)showHideOptions:(id)sender;
- (IBAction)clickBack:(id)sender;
- (IBAction)clickFavorito:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnFavorito;
@property (strong, nonatomic) IBOutlet UILabel *numComentarios;
@property (strong, nonatomic) IBOutlet UITableView *listComentarios;

@end
