//
//  Constants.h
//  manyblogs
//
//  Created by jose garcia orozco on 12/02/13.
//  Copyright (c) 2013 jgarcia. All rights reserved.
//


@interface Constants
#define CELL_WIDHT_PORT 300
#define CELL_WIDHT 240
#define CELL_WIDHT_4 230
#define CELL_HEIGHT 160

#define CELL_WIDHT_IPAD 300
#define CELL_HEIGHT_IPAD 200

#define CELL_GAP_BTN 60
#define CELL_GAP_TOP 60
#define CELL_GAP_LAT_IPH5 30
#define CELL_GAP_LAT_IPH4 5
#define CELL_GAP_LAT_IPAD 35
#define CELL_GAP_LAT_IPAD_PORT 60

#define FLOAT_ANIMATION_DURATION 0.4
#define OFFSET_X 255
#define BLOG_URL @"http://www.aplicacionesipadgratis.com/%@"


#define PAGINA_IPHONE 12
#define PAGINA_IPAD  22



@end
