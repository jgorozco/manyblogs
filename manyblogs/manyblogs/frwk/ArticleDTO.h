//
//  ArticleDTO.h
//  manyblogs
//
//  Created by jose garcia orozco on 13/02/13.
//  Copyright (c) 2013 jgarcia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ArticleDTO : NSObject
{
    NSDictionary *dict;

}

@property(nonatomic,retain)NSString *mainPhoto;
@property(nonatomic,retain)NSString *mainText;
@property(nonatomic,retain)NSArray *photoList;
@property(nonatomic,retain)NSArray *photoListSmall;
@property(nonatomic,retain)NSArray *photoListFull;
@property(nonatomic,retain)NSArray *tags;
@property(nonatomic,retain)NSArray *comments;
@property(nonatomic,retain)NSString *videoURL;
@property(nonatomic,retain)NSString *completeHTMLArticle;
@property(nonatomic,retain)NSString *webUrl;
@property(nonatomic)int numComments;

-(id)initWithDic:(NSDictionary *)dictionary;
+(NSString *)youtubeurl:(NSString *)allcontent;
-(NSDictionary *)toDict;
-(NSString *) toString;
@end
