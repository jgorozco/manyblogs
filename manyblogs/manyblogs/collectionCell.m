//
//  collectionCell.m
//  manyblogs
//
//  Created by jose garcia orozco on 12/02/13.
//  Copyright (c) 2013 jgarcia. All rights reserved.
//

#import "collectionCell.h"
#import "FavManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
@implementation collectionCell
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        // Initialization code
        NSString *nib=@"collectionCell";
        if ([[UIDevice currentDevice] userInterfaceIdiom] != UIUserInterfaceIdiomPhone) {
            nib=@"collectionCell_ipad";
        }
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:nib owner:self options:nil];
        if ([arrayOfViews count] < 1) {return nil;}
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) {
            return nil;
        }
        self = [arrayOfViews objectAtIndex:0];
    }
    return self;
}


-(void)setCachedImage:(UIImage *)image withCache:(BOOL)cached {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
        if (!cached){
            [UIView transitionWithView:[self.imageBg superview]
                              duration:1.0f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                self.imageBg.image = image;
                            } completion:NULL];
        }else{
            self.imageBg.image = image;
        }
        [self.loadingIndicator stopAnimating];
    }];
    
}


-(void)setData:(id)data{
    [self.loadingIndicator startAnimating];
    localData=data;
    [self.textoContenido setText:[localData mainText]];
    [self.numComentarios setText:[NSString stringWithFormat:@"%d",localData.numComments]];
    if (localData.videoURL){
        [self.imgVideo setImage:[UIImage imageNamed:@"video.png"] ];
    }else{
        [self.imgVideo setImage:nil ];
    }
    if ([FavManager isFav:localData.toDict])
    {
        [self.imgFavoritos setImage:[UIImage imageNamed:@"fav.png"]];
    }else{
        [self.imgFavoritos setImage:[UIImage imageNamed:@"nofav.png"]];
    
    }
    
    //    NSLog(@"Loading image:%@",localData.mainPhoto);
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [self.imageBg setImage:[UIImage imageNamed:@"downloading.png"]];
    [manager downloadWithURL:[NSURL URLWithString:localData.mainPhoto]
                    delegate:self
                     options:0
                     success:^(UIImage *image, BOOL cached) {
                         [self setCachedImage:image withCache:cached];
                     } failure:^(NSError *error) {
                         NSLog(@"FAIL IMAGE");
                     }
     ];
}



  
     
     
     
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (IBAction)clickFav:(id)sender {
    
    if ([FavManager isFav:localData.toDict])
    {
        [FavManager remFav:localData.toDict];
        [self.imgFavoritos setImage:[UIImage imageNamed:@"nofav.png"]];
    }else{
        [FavManager addFav:localData.toDict];
        [self.imgFavoritos setImage:[UIImage imageNamed:@"fav.png"]];
        
    }
}
@end
