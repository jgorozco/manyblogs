//
//  COMM.h
//  calendamigos
//
//  Created by jose garcia orozco on 05/11/12.
//  Copyright (c) 2012 technoactivity. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "COMMProtocol.h"
@interface COMM : NSObject


+(BOOL)sendGETQuery:(NSString *)url responseHandler:(NSObject<COMMProtocol> *)handler reqParams:(NSDictionary *)requestParams;
+(BOOL)sendPOSTQuery:(NSString *)url responseHandler:(NSObject<COMMProtocol> *)handler reqParams:(NSDictionary *)requestParams postParams:(NSDictionary *)postParams;

+(NSString *)autoURL:(NSString *)partialUrl params:(NSArray *)params;

+(NSMutableDictionary *)paramsToDict:(NSString *)params;
+ (BOOL)checkConnectivity;
+ (NSString *)getCompleteUrl:(NSString *)origen;

@end
