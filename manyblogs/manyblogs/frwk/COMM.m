//
//  COMM.m
//  calendamigos
//
//  Created by jose garcia orozco on 05/11/12.
//  Copyright (c) 2012 technoactivity. All rights reserved.
//

#import "COMM.h"
#import "Constants.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <CommonCrypto/CommonDigest.h>
#define HOST_SERVER @"google.com"

@implementation COMM


+(void)parseData:(NSData *)data withResponse:(NSURLResponse *)response inHandler:(NSObject<COMMProtocol> *)handler{
   // double mytotalTime=[[NSDate date] timeIntervalSince1970];
    id respuesta=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
   // mytotalTime=[[NSDate date] timeIntervalSince1970]-mytotalTime;
    //NSLog(@"PARSE:TIME::%f     ::%@",mytotalTime,response.URL.absoluteString);
    [handler useResponse:respuesta response:response];

}

+(BOOL)sendGETQuery:(NSString *)url responseHandler:(NSObject<COMMProtocol> *)handler reqParams:(NSDictionary *)requestParams{
    
    NSLog(@"Ejecutando query:%@",url);
     NSString *completeurl=[COMM getCompleteUrl:url];
    if ((![COMM checkConnectivity])&&([[NSFileManager defaultManager] fileExistsAtPath:completeurl])){
        NSData *data=[[NSFileManager defaultManager]contentsAtPath:completeurl];
        //NSLog(@"111111:DATA CACHED::%d:",data.length);
        NSURLResponse *r=[[NSURLResponse alloc] initWithURL:[NSURL URLWithString:url]
                                                   MIMEType:@""
                                      expectedContentLength:data.length
                                           textEncodingName:@""];
        [COMM parseData:data withResponse:r inHandler:handler];
        return YES;
    }
    
    
    NSURL *localUrl = [NSURL URLWithString:url];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:localUrl cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:4.0];
    if ((requestParams)&&(requestParams.allKeys.count>0)){
        for (NSString *key in requestParams.allKeys){
            [urlRequest setValue:[requestParams objectForKey:key] forHTTPHeaderField:key];
        }
    }
 //   [urlRequest setTimeoutInterval:280];
    [urlRequest setHTTPMethod:@"GET"];
    [urlRequest setTimeoutInterval:3000];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            
            if (error){
                
                 NSString *completeurl=[COMM getCompleteUrl:url];
  //              NSLog(@"<-----------%@-------%@",completeurl,url);
                if ([[NSFileManager defaultManager] fileExistsAtPath:completeurl]){
                    //COGEMOS LOS DATOS DE CACHE
                    data=[[NSFileManager defaultManager]contentsAtPath:completeurl];
                    NSURLResponse *r=[[NSURLResponse alloc] initWithURL:[NSURL URLWithString:url]
                                                               MIMEType:@""
                                                  expectedContentLength:data.length
                                                       textEncodingName:@""];
                    [COMM parseData:data withResponse:r inHandler:handler];
                }else{
                    [handler useError:error response:response];
                }
            }else{
                if ((!data)||(data==nil)){
                     NSString *completeurl=[COMM getCompleteUrl:response.URL.absoluteString];
                    if ([[NSFileManager defaultManager] fileExistsAtPath:completeurl]){
                        //COGEMOS LOS DATOS DE CACHE
                        data=[[NSFileManager defaultManager]contentsAtPath:completeurl];
  //                      NSLog(@"DATA CACHED::%d:",data.length);
                     //   id respuesta=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                       // [handler useResponse:respuesta response:response];
                        [COMM parseData:data withResponse:response inHandler:handler];

                    }else{
                        [handler useError:error response:response];
                    }
                     
                    return;
                }else{
                    NSString *completeurl=[COMM getCompleteUrl:response.URL.absoluteString];
  //                  NSLog(@"----%@--------------->:%@",response.URL.absoluteString,completeurl);
                    if ([[NSFileManager defaultManager] fileExistsAtPath:completeurl]){
   //                     NSLog(@"BORRANDO fichero::%@",completeurl);
                        [[NSFileManager defaultManager] removeItemAtPath:completeurl error:nil];
                    }
                    [data writeToFile:completeurl atomically:YES];
                 //   id respuesta=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
       //         NSLog(@"respuesta:::%@",respuesta);
                //    [handler useResponse:respuesta response:response];
                    [COMM parseData:data withResponse:response inHandler:handler];

                }
            }
            
            
        }];
        return YES;
    }
    @catch (NSException *exception) {
        [handler useError:nil response:nil];
        return NO;
    }
}

+ (NSString *)getCompleteUrl:(NSString *)origen
{
    NSData *data = [origen dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
    {
        [output appendFormat:@"%02x", digest[i]];
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    
    return [[paths objectAtIndex:0] stringByAppendingPathComponent:output];
}


+(BOOL)sendPOSTQuery:(NSString *)url responseHandler:(NSObject<COMMProtocol> *)handler reqParams:(NSDictionary *)requestParams postParams:(NSDictionary *)postParams{

    NSURL *localUrl = [NSURL URLWithString:url];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:localUrl];
    if ((requestParams)&&(requestParams.allKeys.count>0)){
        for (NSString *key in requestParams.allKeys){
            [urlRequest setValue:[requestParams objectForKey:key] forHTTPHeaderField:key];
        }
    }
    [urlRequest setTimeoutInterval:3000];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/json; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];

    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    NSError *error;
    NSLog(@"URL:%@",url);
    
    NSData *data=[NSJSONSerialization dataWithJSONObject:postParams
                                                 options:NSJSONReadingMutableContainers
                                                   error:&error];
     NSLog(@"Data:%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    if (error){
        [handler useError:error response:nil];
    }
    
    [urlRequest setHTTPBody:data];
    
    @try {
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
            if (error){
                
                [handler useError:error response:response];
            }else{
       //         NSString *s=[[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
       //         data=[s dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
                id respuesta=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
          //      NSLog(@"Respuesta:%@",(NSDictionary *)respuesta);
                [handler useResponse:respuesta response:response];
            }
            
            
        }];
        return YES;
    }
    @catch (NSException *exception) {
        [handler useError:nil response:nil];
        return NO;
    }
    return NO;
}



+(NSMutableDictionary *)paramsToDict:(NSString *)params{
    NSMutableDictionary *dictParams = [[NSMutableDictionary alloc] init];
    for (NSString *param in [params componentsSeparatedByString:@"&"]) {
        NSArray *elts = [param componentsSeparatedByString:@"="];
        if([elts count] < 2) continue;
            [dictParams setObject:[elts objectAtIndex:1] forKey:[elts objectAtIndex:0]];
    }
    return dictParams;
}


+ (BOOL)checkConnectivity
{
    SCNetworkReachabilityFlags flags;
    BOOL receivedFlags;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(CFAllocatorGetDefault(), [HOST_SERVER UTF8String]);
    receivedFlags = SCNetworkReachabilityGetFlags(reachability, &flags);
    CFRelease(reachability);
    
    return! (!receivedFlags || (flags == 0) );
}



@end
