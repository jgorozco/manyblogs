//
//  ArticleViewController.m
//  manyblogs
//
//  Created by jose garcia orozco on 15/02/13.
//  Copyright (c) 2013 jgarcia. All rights reserved.
//

#import "ArticleViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FavManager.h"
#define FLOAT_ANIMATION_DURATION 0.4
#define OFFSET_X -200
#define OFFSET_Y_IPAD -250

@interface ArticleViewController ()

@end

@implementation ArticleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}
-(void)loadData:(ArticleDTO *)article{
    articulo=article;
}

-(void)viewDidAppear:(BOOL)animated{
    //TODO chequear si esta en favoritos y activar el boton
    if ([FavManager isFav:articulo.toDict]){
        [self.btnFavorito setEnabled:YES];
    }else{
    
    
    }
    
    
    [self.lblTitulo setText:articulo.mainText];
    NSString *urlFile=[[NSBundle mainBundle] pathForResource:@"allwebs.css"
                                                      ofType:nil];
    NSString* content = [NSString stringWithContentsOfFile:urlFile
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
    NSLog(@"--->%@",content);
    NSLog(@"--->YOUTUBE:::%@",articulo.videoURL);
    NSString *str=[NSString stringWithFormat:@"<head><style>%@</style></head><body>%@</body>",content,articulo.completeHTMLArticle];
    [self.webViewContent loadHTMLString:str baseURL:nil];

    [self loadImages:articulo];
    [self.numComentarios setText:[NSString stringWithFormat:@"%d",articulo.numComments]];
    
}




-(void)loadImages:(ArticleDTO *)articulos{
    int posicion=0;
    [[self.scrollViewImages subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    CGRect f=self.scrollViewImages.frame;
    NSString *hasVideo=articulo.videoURL;
    if (hasVideo){
        posicion=posicion+1;
        f=CGRectMake(0, 0, f.size.width, f.size.height);
        UIWebView *video=[[UIWebView alloc] initWithFrame:f];
        video.scrollView.scrollEnabled = NO;
        video.scrollView.bounces = NO;
        [video loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:hasVideo]]];
        [self.scrollViewImages addSubview:video];
    }
    
    
    for (NSString *elem in articulo.photoList){
        f=CGRectMake(posicion*f.size.width, 0, f.size.width, f.size.height);
        UIImageView *view=[[UIImageView alloc]initWithFrame:f];
        [view setContentMode:UIViewContentModeScaleAspectFill];
        [view setImage:[UIImage imageNamed:@"downloading.png"]];
        [self.scrollViewImages addSubview:view];
        posicion=posicion+1;
        [[SDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:elem]
                                                  delegate:self
                                                   options:0
                                                   success:^(UIImage *image, BOOL cached) {
                                                       [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                                                           if (!cached){
                                                               [UIView transitionWithView:self.scrollViewImages
                                                                                 duration:1.0f
                                                                                  options:UIViewAnimationOptionTransitionCrossDissolve
                                                                               animations:^{
                                                                                   view.image = image;
                                                                               } completion:NULL];
                                                           }else{
                                                               view.image = image;
                                                           }
                                                       }];
                                                       
                                                   } failure:^(NSError *error) {
                                                       NSLog(@"FAIL IMAGE");
                                                   }
         ];
    }
    [self.scrollViewImages setContentSize:CGSizeMake(f.size.width*posicion, f.size.height)];
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)showHideOptions:(id)sender {
    int offset=OFFSET_X;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        offset=OFFSET_Y_IPAD;
    }
    
    if (self.vistaContenido.frame.origin.x==0.0){
        [UIView animateWithDuration:FLOAT_ANIMATION_DURATION
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             [self.vistaContenido setFrame:CGRectMake(offset, 0,
                                                                      self.vistaContenido.frame.size.width,
                                                                      self.vistaContenido.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                         }];
        
        
        
    }else{
        [UIView animateWithDuration:FLOAT_ANIMATION_DURATION
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             [self.vistaContenido setFrame:CGRectMake(0, 0,
                                                                      self.vistaContenido.frame.size.width,
                                                                      self.vistaContenido.frame.size.height)];
                         }
                         completion:^(BOOL finished){
                             NSLog(@"Done!");
                         }];
        
        
        
        
    }
    
    
}

- (IBAction)clickBack:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
    
}

- (IBAction)clickFavorito:(id)sender {
    //si esta en favoritos, quitarlo, si no esta, aniadirlo
    if ([FavManager isFav:articulo.toDict]){
        [self.btnFavorito setEnabled:NO];
        [FavManager remFav:articulo.toDict];
    }else{
        [self.btnFavorito setEnabled:YES];
        [FavManager addFav:articulo.toDict];
    }
}


- (void)viewDidUnload {
    [self setLblTitulo:nil];
    [self setScrollViewImages:nil];
    [self setWebViewContent:nil];
    [self setVistaContenido:nil];
    [self setBtnFavorito:nil];
    [self setNumComentarios:nil];
    [self setListComentarios:nil];
    [super viewDidUnload];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return articulo.numComments;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"commentCellView" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    NSDictionary *object = [articulo.comments objectAtIndex:indexPath.row];
//    NSLog(@"comentaro:%@",object);
    NSArray *subvistas=[[cell.subviews objectAtIndex:0] subviews];//[[cell.subviews objectAtIndex:0] subviews];
    for (UIView *v in subvistas){
        switch (v.tag) {
            case 10:{
                UILabel *lbl=(UILabel *)v;
                NSString *s=[object objectForKey:@"name"];
                if ((s)&&(![s isEqual:[NSNull null]])&&(s!=NULL))
                    [lbl setText:s];
            }
                break;
            case 11:{
                UILabel *lbl=(UILabel *)v;
                NSString *s=[object objectForKey:@"content"];
                if ((s)&&(![s isEqual:[NSNull null]])&&(s!=NULL))
                    [lbl setText:[self stringByStrippingHTML:s]];
            }
                break;
            default:
                break;
        }
        
        
    }
    
    
    return cell;
}

-(NSString *) stringByStrippingHTML:(NSString *)incoming {
    NSRange r;
    NSString *s = incoming;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if ([request.URL.absoluteString isEqualToString:@"about:blank"]){
        return YES;
    }else{
        if ([ArticleDTO youtubeurl:request.URL.absoluteString]){
            return YES;
        }else{
            [[UIApplication sharedApplication] openURL:request.URL];
            return NO;
        }
       
    }
    if ([ArticleDTO youtubeurl:request.URL.absoluteString]){
        return YES;
    }

    return NO;
}





@end
