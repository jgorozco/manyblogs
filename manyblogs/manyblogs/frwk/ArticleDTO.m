//
//  ArticleDTO.m
//  manyblogs
//
//  Created by jose garcia orozco on 13/02/13.
//  Copyright (c) 2013 jgarcia. All rights reserved.
//

#import "ArticleDTO.h"

@implementation ArticleDTO
@synthesize videoURL,completeHTMLArticle,mainPhoto,mainText,numComments,photoList,webUrl,tags,photoListSmall,photoListFull,comments;

-(id)initWithDic:(NSDictionary *)dictionary{
    dict=dictionary;
    mainText=[dictionary objectForKey:@"title"];
    completeHTMLArticle=[dictionary objectForKey:@"content"];
    numComments=[[dictionary objectForKey:@"comments"] count];
    comments=[dictionary objectForKey:@"comments"];
    webUrl=[dictionary objectForKey:@"url"];
    tags=[dictionary objectForKey:@"tags"];
    videoURL=[ArticleDTO youtubeurl:completeHTMLArticle];
    NSMutableArray *tempPhotoList=[[NSMutableArray alloc] init];
    NSMutableArray *tempPhotoListSmall=[[NSMutableArray alloc] init];
    NSMutableArray *tempPhotoListFull=[[NSMutableArray alloc] init];
    NSArray *adjuntos=[dictionary objectForKey:@"attachments"];
    //
    for (NSDictionary *adjunto in adjuntos){
        NSString *type=[adjunto objectForKey:@"mime_type"];
        NSDictionary *images=[adjunto objectForKey:@"images"];
        if ([type hasPrefix:@"image"]){
            if (!mainPhoto){
                mainPhoto=[[images objectForKey:@"medium"] objectForKey:@"url"];
            }
            [tempPhotoList addObject:[[images objectForKey:@"large"] objectForKey:@"url"]];
            [tempPhotoListSmall addObject:[[images objectForKey:@"medium"] objectForKey:@"url"]];
            [tempPhotoListFull addObject:[adjunto objectForKey:@"url"]];
        }
    }
    if (!mainPhoto){
        mainPhoto=[dictionary objectForKey:@"thumbnail"];
    }
    
    photoList=tempPhotoList;
    photoListSmall=tempPhotoListSmall;
    photoListFull=tempPhotoListFull;
    
    return self;
}

-(NSDictionary *)toDict{
    return dict;
}
-(NSString *) toString{
    return [NSString stringWithFormat:@"%@",dict];


}


+(NSString *)youtubeurl:(NSString *)allcontent{
    NSError *error = NULL;
    NSString *regexString = @"youtube.[a-z]+/embed/([-a-zA-Z0-9_]+)";
    NSRegularExpression *regex =
    [NSRegularExpression regularExpressionWithPattern:regexString
                                              options:NSRegularExpressionCaseInsensitive
                                                error:&error];
    NSTextCheckingResult *match = [regex firstMatchInString:allcontent
                                                    options:0
                                                      range:NSMakeRange(0, [allcontent length])];
    NSString *substringForFirstMatch;
    if (match) {
        NSRange videoIDRange = [match rangeAtIndex:1];
        substringForFirstMatch = [allcontent substringWithRange:videoIDRange];
        return [NSString stringWithFormat:@"http://youtube.com/embed/%@",substringForFirstMatch];
    }else{
        
        return nil;
    }
    
    
}


@end
