//
//  collectionCell.h
//  manyblogs
//
//  Created by jose garcia orozco on 12/02/13.
//  Copyright (c) 2013 jgarcia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ArticleDTO.h"
@interface collectionCell : UICollectionViewCell
{
    ArticleDTO *localData;

}

-(void)setData:(id)data;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (strong, nonatomic) IBOutlet UIImageView *imageBg;
@property (strong, nonatomic) IBOutlet UILabel *textoContenido;
@property (nonatomic, strong) id data;
@property (weak, nonatomic) IBOutlet UILabel *numComentarios;
@property (weak, nonatomic) IBOutlet UIImageView *imgVideo;
@property (weak, nonatomic) IBOutlet UIImageView *imgFavoritos;
- (IBAction)clickFav:(id)sender;


@end
